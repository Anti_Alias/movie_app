// Imports modules
const fs = require('fs')
const sqlite3 = require('sqlite3').verbose();
const basic = require('./app/controller/basic');
const dbutil = require('./app/db/util');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

// Gets sqlite3 db connection.
// Initializes database if it does not exist.
const db = dbutil.init('./db/movies.db')

// Gets express api instance.
const app = express();

// Configures routes
app.use(express.static('static'))   // Routing of static files
app.use(bodyParser.json())
basic.route(app, db)                // Routing of pages

// Configures views
const port = 3000;
app.locals.pretty = true;
app.set('views', path.resolve("./view"));
app.set('view engine', 'pug');

// Starts server
app.listen(port, () => {
    console.log('Application listening on port ' + port + '!');
});
