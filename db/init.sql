CREATE TABLE IF NOT EXISTS movie
(
    id integer PRIMARY KEY,
    title text NOT NULL UNIQUE,
    image text NOT NULL,
    releaseDate TIMESTAMP NOT NULL,
    rating integer NOT NULL
);
