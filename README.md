# Movie Rating App

## Description
This is a NodeJS application that can display a set of movies backed by an SQLite3 database.
When running, a list of movies will appear with their title, release date and most importantly,
their review score (1 - 5 stars). Movies with 0 stars are considered not yet rated.

Users can click on the stars. When they do, the rating for the movie is update in the SQLite3 database
and  the user is notified of the successful transaction. An error message will display in the event
of failure. Users cannot modify movies that are not yet rated.

When running the app for the first time, the database schema is generated and the initial data
set in ./db/movies.json is consumed. This does not happen in subsequent runs unless the database file is removed/deleted.


## Application Prerequisites
    1) Install nodejs v8.11.1 or higher.
    2) Install npm 5.6.0 or higher. (If installer has not already done so.)

## Running Locally
    1) npm install
    2) npm test
    3) Navigate browser to localhost:3000

## Technologies Used
* NodeJS
* Express
* SQLite3
* Bootstrap
* jQuery
* Font Awesome

## Caveats
* In order to demonstrate the functionality of not allowing the user to modify an unreleased movie, I had to alter the input json provided since
the movies with no rating were 2015 or prior.
* Due to time constraints, I was not able to implement unit tests for my backend logic.
* Filtering movies by different criteria was cut for the same reason.
* Since there is no authentication in place, there is nothing stopping a user from updating the rating of a movie that is not yet rated using a post request through curl.
