/**
* Represents the entire API for the website.
*/
function Site() {

    /**
    * Initializes the website
    */
    this.init = function() {

        // Gets star widgets
        const starWidgets = $('.stars');

        // Configures star widgets.
        starWidgets.each((index, widget)=> {

            // Adds base star widget functionality.
            site.buildStarWidget(widget);

            // Attaches listener for when rating changes.
            // Makes post request to application in order
            // to change the movie's rating.
            widget.onChangeRating((rating)=>{
                const title = widget.getAttribute('title');
                const data = { 'title' : title, 'rating' : rating }
                $.ajax({
                    type : 'POST',
                    url : '/movie/' + title,
                    contentType: 'application/json',
                    data : JSON.stringify(data),
                    success : (message) => {
                        const wrapped = '<p class="success-text">' + message + '</p>'
                        widget.displayText(wrapped);
                    },
                    error : (xhr, status, message) => {
                        let text = xhr.responseText;
                        if(text == undefined) text = 'Could not connect to server.'
                        console.log(text);
                        const wrapped = '<p class="failure-text">' + text  + '</p>'
                        widget.displayText(wrapped);
                    }
                });
            });
        });
    }


    /**
    * Builds a star widget
    */
    this.buildStarWidget = function(widget) {

        // Gets jquery widget
        const jwidget = $(widget);

        // Adds widget ability to invoke callbacks.
        widget.listeners = []
        widget.onChangeRating = function(callback) {
            widget.listeners.push(callback);
        }

        // Allows widget to display arbitrary messages
        widget.displayText = function(text) {
            jwidget.find('.star-message').html(text);
        }

        /**
        * Widget change handler.
        * When invoked, populates stars in a way that reflects
        * the review.
        * @param {integer} rating Ratingo of the movie in stars (1-5)
        */
        widget.change = function(rating) {

            // Appends message if not rated.
            jwidget.html('');

            // Gets date information of movie and current date.
            let releaseDateStr = jwidget.attr('releaseDate');
            const [year, month, day] = releaseDateStr.split("-");
            let releaseDate =  new Date(year, month - 1, day);
            let now = new Date();
            let isRated = (releaseDate <= now);

            // Adds stars
            for(i=1; i<=5; i++) {

                // Determines class string. Either solid or not.
                let classStr = "";
                if(i <= rating) classStr = ' class="fa fa-star'
                else classStr = ' class="far fa-star'
                if(rating > 0) classStr += ' button"'
                else classStr += '"'

                // Determines rating callback string.
                // Null if there release date is after today.
                let onClickStr = "";
                if(isRated) {
                    onClickStr = ' onclick="this.parentNode.change(' + i + ');"'
                }

                // Determines the amount of the star (1-5)
                // Also determines movie.
                const amountStr = ' amount="' + i + '"'

                // Appends a star
                jwidget.append('<i ' + classStr + amountStr + onClickStr + '></i>');
            }

            // Adds 'not rated' if zero stars
            if(!isRated) {
                jwidget.append('<p>Not Rated</p>');
            }
            else {
                // Adds container for messages. This comes in handy when we want to update information about the movie.
                jwidget.append('<div class="star-message"></div>')
            }

            // Invokes listeners
            for(callback of widget.listeners) {
                callback(rating)
            }
        }

        // Invokes change the first time.
        widget.change(widget.getAttribute('rating'));
    }

    /**
    * Invoked when star widget changes.
    */
    this.addStars = function(widget, rating) {

        // Gets jquery widget
        const jwidget = $(widget);
    }
}

// Global instance of site.
var site = new Site()

// Runs once page is loaded
$(document).ready(()=> {

    // Runs website code
    site.init();
});
