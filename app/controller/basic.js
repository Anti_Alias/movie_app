
/**
* Module that is responsible for routing basic pages
* in an express application.
*/
function Basic(db) {

    /**
    * Routes an express application
    * @param {Object} app Express application to route.
    * @param {Object} db Sqlite3 database connection
    */
    this.route = function(app, db) {

        // GET home page
        app.get('/', (req, res) => {
            db.all('SELECT * FROM movie', (err, rows) => {
                res.render('index.pug', { title : 'Movie Verdicts', 'movies' : rows } );
            });
        });

        // GET movie json
        app.get('/movie/:title', (req, res) => {
            const title = req.param('title');
            db.all('SELECT * FROM movie WHERE title=?', [title], (err, rows) => {
                if(rows.length > 0) {
                    const movie = rows[0]
                    delete movie.id
                    res.send(JSON.stringify(movie));
                }
                else {
                    res.status(400);
                    res.send("Movie not found");
                }
            })
        });

        // POST movie json
        app.post('/movie/:title', (req, res) => {
            const title = req.param('title');
            const body = req.body;
            const query = 'UPDATE movie SET rating=? WHERE title=?';
            const stmt = db.run(query, [body.rating, body.title], function(err) {
                if(err || this.changes !== 1) {
                    res.status(400);
                    res.send("Could not update movie.");
                }
                else {
                    res.send("Updated!");
                }
            })
        })
    }
};

// Exports module
module.exports = new Basic()
