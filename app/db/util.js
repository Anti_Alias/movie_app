const fs = require('fs')
const sqlite3 = require('sqlite3').verbose();

/**
* Module that is responsible for routing basic pages
* in an express application.
*/
function Util(db) {

    /**
    * Initializes database if one has not already been created.
    * @param {string} dbPath Path to sqlite3 database relative to root of node js application.
    * Initialzes database file if it does not exist.
    * @return Connection to that database.
    */
    this.init = function(dbPath) {

        // Checks existance of DB file
        const dbExists = fs.existsSync(dbPath);

        // Creates DB connection
        const db = new sqlite3.Database(dbPath);

        // If DB did not exist, initialize it.
        if(!dbExists) {
            console.log("Database was not found. Initializing.");

            // Loads init SQL script and initial movies to load
            const initMovies = JSON.parse(fs.readFileSync('./db/movies.json', 'utf-8'));
            const initScript = fs.readFileSync('./db/init.sql', 'utf-8');

            // Connects to SQLite DB and runs initialization script.
            // Also inserts all movies
            db.serialize(() => {

                // Runs init script
                db.run(initScript);

                // Inserts records
                const stmt = db.prepare('INSERT INTO movie (title, image, rating, releaseDate) VALUES (?, ?, ?, ?)');
                for(const movie of initMovies) {
                    stmt.run(movie.title, movie.image, movie.rating, movie.releaseDate);
                }
            });
        }
        else {
            console.log("Database found.");
        }

        // Returns database connection
        return db;
    }
};

// Exports module
module.exports = new Util()
